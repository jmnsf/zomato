class AddNotesToRestaurants < ActiveRecord::Migration
  def change
    add_column :restaurants, :text_notes, :text
  end
end
