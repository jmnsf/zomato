class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :zomato_id, index: true, unique: true

      t.timestamps null: false
    end
  end
end
