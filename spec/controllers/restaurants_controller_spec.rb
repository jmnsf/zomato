require 'rails_helper'

RSpec.describe RestaurantsController, type: :controller do

  describe "GET #search" do
    it "returns http success" do
      get :search
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #attachments" do
    it "returns http success" do
      get :attachments
      expect(response).to have_http_status(:success)
    end
  end

end
