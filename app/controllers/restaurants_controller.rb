class RestaurantsController < ApplicationController
  respond_to :js

  def search
    fetch_restaurants
    save_search_params
    @rests = clean_restaurants
    fill_restaurant_models @rests
  end

  def filter
    bookmark_ids = ZomatoApi::Restaurants.bookmarks
    @to_remove = params[:shown_ids].split(',') - bookmark_ids
  end

  def create
    Restaurant.create! restaurant_params
    redirect_to :root
  end

  def update
    Restaurant.find(params[:id]).update! restaurant_params
    redirect_to :root
  end

  private

  def save_search_params
    sp = search_params
    cookies.signed['last-query'] = sp[:query]
    cookies.signed['last-lat'] = sp[:latitude]
    cookies.signed['last-lon'] = sp[:longitude]
  end

  def fill_restaurant_models rests
    saved = Restaurant.where(zomato_id: @rests.map { |r| r['id'] }).attachs
      .inject({}) do |ret, r|
        ret.merge({ r.zomato_id => r })
      end

    rests.each do |r|
      r['rest_obj'] = saved[r['id']] || Restaurant.new
      r['rest_obj'].attachments.build
    end
  end

  def fetch_restaurants
    @zomato_restaurants = JSON.parse ZomatoApi::Restaurants.search search_params
  end

  def clean_restaurants
    @zomato_restaurants['restaurants'].map { |r| r['restaurant'] }
  end

  def search_params
    ps = query_params
    ps[:limit] ||= 20
    ps
  end

  def query_params
    params.permit :query, :latitude, :longitude
  end

  def restaurant_params
    params.require(:restaurant)
      .permit(:zomato_id, :text_notes, attachments_attributes: :file)
  end
end
