# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

searchTimer = 0
searchTimeout = 500
timeoutId = null

locationLat = null
locationLon = null

clearFilter = (e) ->
  $('#clear-filter').addClass 'hidden'
  $('.restaurant-container').show()

searchRestaurants = ->
  timeoutId = null
  $('#main-spinner').show()
  $('#restaurant-list-container').hide()
  $('#clear-filter').addClass 'hidden'
  $('#restaurant-search').parents('form').submit()

searched = ->
  window.clearTimeout(timeoutId) if timeoutId != null
  timeoutId = window.setTimeout searchRestaurants, searchTimeout

getLocation = ->
  navigator.geolocation.getCurrentPosition (position) ->
    locationLat = position.coords.latitude
    locationLon = position.coords.longitude
    $('input#latitude').val locationLat
    $('input#longitude').val locationLon

window.zmt.fileSelected = (event, numFiles, label) ->
  return unless numFiles > 0
  span = $(event.target).parent()
  span.removeClass 'btn-primary'
  span.addClass 'btn-success'
  span.siblings('button').removeClass 'hidden'

$(document).ready ->
  $('#restaurant-search').on 'keydown', searched
  $('#clear-filter').on 'click', clearFilter
  do getLocation
  do searchRestaurants
