class Attachment < ActiveRecord::Base
  belongs_to :restaurant

  has_attached_file :file, styles: { thumb: "100x100>" }

  validates_attachment :file, presence: true,
    content_type: { content_type: /\Aimage\/.*\Z/ }
end
