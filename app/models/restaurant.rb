class Restaurant < ActiveRecord::Base
  has_many :attachments

  accepts_nested_attributes_for :attachments

  validates :zomato_id, presence: true, uniqueness: true

  scope :attachs, -> { joins(:attachments).includes(:attachments) }
end
