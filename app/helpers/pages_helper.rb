module PagesHelper
  def last_query
    cookies.signed['last-query']
  end

  def last_latitude
    cookies.signed['last-lat']
  end

  def last_longitude
    cookies.signed['last-lon']
  end
end
