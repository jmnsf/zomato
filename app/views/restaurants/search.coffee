container = $('#restaurant-list-container')
container.html('')

<% @rests.each do |rest| %>

container.append('<%= j render(partial: "restaurant_badge", locals: { restaurant: rest }) %>')

<% end %>

$('#main-spinner').hide()
container.show()

$('#filter-restaurants-input').val('<%= @rests.map { |r| r["id"] }.join(",") %>')

$('.attachment-container').each ->
  $(this).magnificPopup({
    delegate: 'a'
    type: 'image'
    gallery: { enabled: true }
  })

$('.btn-file :file').on 'fileselect', window.zmt.fileSelected
$('.btn-file :file').on 'change', () ->
  input = $(this)
  numFiles = if input.get(0).files.length then input.get(0).files.length else 1
  label = input.val()
  input.trigger('fileselect', [numFiles, label])
