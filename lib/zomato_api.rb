module ZomatoApi
  API_KEY = ENV['ZOMATO_API_KEY']
  BASE_URL = 'https://developers.zomato.com'
  API_VER = '/api/v2.1'

  ZOMATO_URL = 'https://www.zomato.com'
  BOOKMARKS_PATH = '/php/filter_user_tab_content.php'

  class Restaurants
    def self.search(opts = {})
      puts "FETCHING RESTAURANTS #{params(opts).to_s}"
      Excon.get(BASE_URL,
        path: path('/search'), headers: headers, query: params(opts)
      ).body
    end

    def self.bookmarks(opts = {})
      puts "FETCHING BOOKMARKS #{opts.to_s}"
      doc = Nokogiri::HTML(
        JSON.parse(
          Excon.post(ZOMATO_URL,
            path: BOOKMARKS_PATH,
            body: bookmarks_body(opts),
            headers: { 'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8' }
          )
          .body
        )['html']
      )
      doc.css('.snippet-restaurant').map do |rest|
        rest.attribute('data-res-id').value
      end
    end

    private

    def self.headers
      {
        user_key: API_KEY,
        'Accept' => 'application/json'
      }
    end

    def self.path(path)
      API_VER + path
    end

    def self.params(opts = {})
      ret = {}
      ret[:q] = opts[:query] if opts[:query].present?
      ret[:lat] = opts[:latitude] if opts[:latitude].present?
      ret[:lon] = opts[:longitude] if opts[:longitude].present?
      ret[:start] = opts[:offset] if opts[:offset].present?
      ret[:count] = opts[:limit] if opts[:limit].present?
      ret[:radius] = 10000
      ret[:sort] = 'real_distance'
      ret[:order] = 'asc'
      ret
    end

    def self.bookmarks_body(opts = {})
      URI.encode_www_form(
        user_id: 3188066,
        limit: 10,
        tab: 'bookmarks'
      )
    end
  end
end
